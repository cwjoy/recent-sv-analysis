---
title: "Recent Sexual Violence and HIV risk Analysis"
output:
  pdf_document:
    toc: yes
---

```{r headnote, include=FALSE}
# This Markdown file, created by Jason, replaces "Analysis.R". This excludes 
# exploratory code that will not be used for the manuscript; therefore, the
# original R code should be kept as documentation. 
# Added to code repository 5/2/19 by AMA.
# -
# - Reads in "data/Recent_SV_Analysis_ModelXX.rds"
# - Creates Table 1
# - Creates Table 2
# - Creates heatmaps
```

```{r libaries, include=FALSE}
# install.packages("summarize", repos="http://R-Forge.R-project.org")
# library(summarize)
# Edit
# Edit2

library(StatAnalysis)
library(ggsignif)
library(knitr)
#library(xtable)
library(tidyverse)
library(kableExtra)
library(magick)
library(webshot)

library(tableone)
library(reshape2)

```


```{r import cleaned code, include=FALSE}
# Import dataset
# Note 6/24/19: Chosen model is 14. (For sensitivity analysis, use model 15.)
ds <- readRDS(file='data/Recent_SV_Analysis_Model14.rds')

# classifying depression and menstrual phas
ds$depressed <- ifelse(ds$DepressionIndex > 15, 1,0)
ds$phase <- factor(ifelse(ds$PlasmaProgesterone > 0.14, "Follicular" , "Luteal" ))

#Create new logical variables from yes/no predictor variable
for (n in colnames(ds)){
  if (is.element("Yes", ds[,n]) == TRUE){
    ds[,paste0(n, "_log")] <- ifelse(ds[,n]=="Yes", TRUE, FALSE)
  } 
}

#Create new logical variables from pos/neg predictor variable
for (n in colnames(ds)){
  if (is.element("Positive", ds[,n]) == TRUE){
    ds[,paste0(n, "_log")] <- ifelse(ds[,n]=="Positive", TRUE, FALSE)
  } 
}

# subsetting cases and controls 
control <- subset(ds, case_status != "Case")
case <- subset(ds, case_status == "Case")

use(ds)

```

```{r revalue some factors, include=FALSE}
# Relabeling some responses ----
levels(ds$thirty_days_partners)
ds$thirty_days_partners <- recode(ds$thirty_days_partners, 
                                   "Not specified" = "No response")

levels(ds$vaginal_condom)
ds$vaginal_condom <- recode(ds$vaginal_condom,
                             "Not applicable/not specified" = "No response")

# Changing levels of factors ----
## Change levels so "Yes" prints first
for (n in colnames(ds)){
  if (is.element("Yes", ds[,n]) == TRUE){
    ds[,n] <- relevel(ds[,n] , "Yes")
  } 
}

## Change order of Race
ds$Race <- factor(ds$Race, c("Black", "White", "Other"))

## Change order of Insurance
ds$Insurance <- factor(ds$Insurance, c("Private", "Public", "None"))

## Change levels so "Positive" prints first
for (n in colnames(ds)){
  if (is.element("Positive", ds[,n]) == TRUE){
    ds[,n] <- relevel(ds[,n] , "Positive")
  } 
}

rm(n)

```
\newpage

# Table one
```{r table one, echo=FALSE, results='asis'}
# Construct table one
stOptions(sigHighlight='bold')
statTable(data=ds,
          caption="Demographic and Clinical Characteristics",
          columns={
            stats(by=ds$case_status)
            pvalue(by=ds$case_status,annotate = TRUE, critValue = 0.05)
          },
          rows={
            'N': .n.
            # "": heading.           
            "Demographic Characteristics": heading.
            "Age": Age %as% stats(method='medq') + pvalue(method='rank')
            "Race": Race
            "Insurance": Insurance
            # "": heading.
            
            "Depression and Abuse Information": heading.
            "Depression (CESD) Score": DepressionIndex %as% stats(method='medq') + pvalue(method='rank')
            
            "Emotional abuse ever": abuse_ever_emot_log
            "Emotional abuse past month": abuse_month_emot_log
            "Physical abuse ever": abuse_ever_phys_log
            "Physical abuse past month": abuse_month_phys_log
            "Sexual abuse ever": abuse_ever_sex_log
            "Sexual abuse past month": abuse_month_sex_log
            # "": heading.
                        
            "Reproductive Status and Sexual Behavior": heading.
            "Menstrual phase": phase
            "On Hormone contraceptive": Hormone_modulator_log
            "Non-hormone contraceptive": Other_contraceptive_log
            "Any RTI*": rti_any_log
            "Bacterial Vaginosis": bv_log
            'Candida': candida_log
            
        # Not used:
            #"Days from LMP": days_from_lmp
            #"Last experienced Emotional abuse": emotional_when
            #"Duration of Emotional Abuse": emotional_duration
            #"Last experienced physical abuse": physical_when
            #"Duration of physical Abuse": physical_duration
            #"Mental health medication": Mental_hlth_med
            #"Pain medication": Pain_medication
            #"On Antibiotics": Antibiotic
            #"On other chronic illness drug": Chr_illness_med
            #"Takes recreational drugs": Recreational_drug
            #"Uses Vaginal Lubricant": Vaginal_lubricant
            # "Used vaginal wash": Vaginal_wash
            #"Last experienced sexual abuse": sexual_when
            #"Duration of sexual Abuse": sexual_duration
            # "Minimum days from event": min_days_from_event %as% pvalue(method='none')
            #"Max days from event": max_days_from_event %as% pvalue(method='none')
            #"gonorrhea (negative)": gc
            #"Other RTI": rti_other
            #"Chlamydia": ct
            #"Trichomonas": trich
          })

# table <- do.call("rbind", table)
# print(kable(table, 
#               format = "pandoc", 
#               caption = "test"))

```
\newpage
# Table two
```{r table two setup, include=FALSE}
# Getting biomarker names for table two ----
biomarkers <- c(grep('CVL|Plasma' , names(ds) , value = TRUE))
biomarkers <- biomarkers[!biomarkers %in% c("PlasmaProgesterone",
                                            'PlasmaEstradiol')]

# Setting biomarker order
bioOrder <- c("TNFa", "IL6", "IL1a", "IL1b", "TGFB", "MIP3a", "IL8", "MCP1", "IP10", "SLPI", "Elafin", "HBD2", "TSP1", "VEGF", "PDGF", "FGF", "CathepsinB", "MMP1", "MMP2", "MMP7", "MMP8", "MMP9", "TIMP1", "TIMP2", "CRP", "Protein")

vals <- NULL
for (n in bioOrder){
  b <- print(grep(n, biomarkers, value = TRUE))
  vals <- c(vals, b)
  rm(b)
  rm(n)
}

biomarkers <- vals

# Subset categorical and Continous biomarkers
catBiomarkers <- c("CVLTGFB", "CVLIP10", "CVLPDGF","CVLFGF", "CVLMMP1", "CVLMMP2",  
                   "PlasmaIL1b", "PlasmaIP10")

nonnormBiomarkers <- biomarkers[!biomarkers %in% catBiomarkers]

# Summary statistics, continuous variables ----------
## Function for the median and IQR stats for the table, 
medIQR <- function(x){
          q <- format(round(median(x), digits = 2), nsmall = 2)
          r <- format(round(quantile(x, probs = c(0.25, 0.75), na.rm = TRUE), digits = 2), nsmall = 2)
          s <- paste0(q," [", r[1], " - ", r[2], "]")
          return(s) 
        }

## Making the table
conTab <- ds %>%
           select(c(status,nonnormBiomarkers)) %>%
           group_by(status) %>%
           summarise_at(nonnormBiomarkers, funs(medIQR)) %>%
           select(status, sapply(nonnormBiomarkers, contains)) %>%
           t() %>%
           as.data.frame() 

## Clean the table and make a column
colnames(conTab) <- as.character(unlist(conTab[1,]))
conTab <- conTab[-1,]
conTab$Class <- "Continuous: log(pg/ml)"

# Summary statistics, categorical variables ----------
## Function for the number and pct detectable stats for the table, 
nPCT <- function(x){
  n <- length(x)
  dets <- x == "1"
  dets <- sum(dets, na.rm = TRUE)
  pct <- (dets / n) * 100
  pct <- round(pct, digits = 1)
  s <- paste0(dets," [",pct,"]")
  return(s) 
}

## Making the table
catTab <- ds %>%
  select(c(status,catBiomarkers)) %>%
  group_by(status) %>%
  summarise_at(catBiomarkers, funs(nPCT)) %>%
  select(status, sapply(catBiomarkers, contains)) %>%
  t() %>%
  as.data.frame() 


## Clean the table and make a column
colnames(catTab) <- as.character(unlist(catTab[1,]))
catTab <- catTab[-1,]
catTab$Class <- "Categorical: Detectable [%]"

# Merge two tables together together
t2 <- rbind(conTab, catTab)
t2$Biomarker <- rownames(t2)


# Model: Biomarker = Case Status weighted by Propensity score ----
contDF <- lapply(nonnormBiomarkers, function(x) lm(ds[[x]]~ds$case_status, weights = ds$iptw)) %>%
  lapply(., summary) %>%
  sapply(., function(x) x$coefficients[,c(1,4)]) %>%
  as.data.frame(.)

## Labeling and transforming table
rownames(contDF) <- c("Intercept Estimate", "Case Estimate",
                      "Intercept Pvalue", "CasePvalue")
colnames(contDF) <- nonnormBiomarkers
contDF <- contDF[-c(1,3),]
contDF <- as.data.frame(t(contDF))

# Model: Biomarker = Case Status weighted by Propensity score ----
catDF <- lapply(catBiomarkers , 
                function(x) glm(ds[[x]]~ds$case_status, weights = ds$iptw, family = "binomial")) %>%
  lapply(., summary) %>%
  sapply(., function(x) x$coefficients[,c(1,4)]) %>%
  as.data.frame(.)

## Labeling and transforming table
rownames(catDF) <- c("Intercept Estimate", "Case Estimate",
                     "Intercept Pvalue", "CasePvalue")
colnames(catDF) <- catBiomarkers
catDF <- catDF[-c(1,3),]
catDF <- as.data.frame(t(catDF))
catDF$`Case Estimate` <- exp(catDF$`Case Estimate`)

## p-value adjustments
caseDF <- rbind(contDF, catDF)
caseDF$CasePvalueBY <- p.adjust(caseDF$CasePvalue , method = 'BY')
# caseDF$CasePvalueFDR <- p.adjust(caseDF$CasePvalue , method = 'fdr')

# Merging everything together ----
caseDF$Biomarker <- rownames(caseDF)
t2 <- full_join(t2, caseDF)
rownames(t2) <- t2$Biomarker

write.csv(caseDF, "tables/RSVmodels.csv")
rm(caseDF)

### All data is now in table 2
```

```{r Organize table 2, include=FALSE}
# Organize table 2 ---
## Set levels for Class column 
t2$Class <- as.factor(t2$Class)
t2$Class <- relevel(t2$Class, unique(grep("Continuous", t2$Class, value = TRUE)))

## Separate Biomarker column into sample type and biomarker
t2 <- separate(t2, Biomarker, c("Type","Biomarker"), sep ="(?<=CVL)|(?<=Plasma)", remove = FALSE)

# Reorder table 2 ---
order <- c("Type","Class","Biomarker", "Control", "Case", "Case Estimate", "CasePvalue", "CasePvalueBY")
t2 <- t2[,order]
t2 <- t2[order(t2$Type, t2$Class),]
t2 <- t2 %>%
      rename(`Control (N=25)` = Control,
              `Case (N=13)` = Case)

## If I need to reorder the biomarkers I'll probably need to do that here

## Rename categorical rows
write.csv(t2, "tables/Table2.csv")
# Get rid of what we don't need
rm( catBiomarkers, nonnormBiomarkers, 
   order)

## Table two - DONE
```

```{r print table two, echo=FALSE}
options(knitr.kable.NA = '')
colnames <- c("","","","Control (N=25)", "Case (N=13)", 
              paste("Case Estimate", footnote_marker_alphabet(1)), 
              paste("P-value", footnote_marker_alphabet(2)), 
              paste("Adjusted P-value", footnote_marker_alphabet(3)))

# Table output
t2 %>%
  mutate(
  Type = cell_spec(Type),
  Class = cell_spec(Class),
  #Biomarker = cell_spec(Biomarker, 
                        #align = ifelse(grepl("(%)", 
                                             #Biomarker,ignore.case = TRUE) == TRUE,"r","l")),
  #Control = cell_spec(Control),
  #Case =  cell_spec(Case),
  `Case Estimate` = cell_spec(round(`Case Estimate`,2),
                          color = ifelse(is.na(CasePvalue) == TRUE, "white", "black"),
                          align = "c"
                          ),
  CasePvalue =  cell_spec(round(CasePvalue, 4), 
                          bold = ifelse(CasePvalue <= 0.05, TRUE, FALSE),
                          color = ifelse(is.na(CasePvalue) == TRUE, "white", "black"),
                          align = "c"
                          ),
  CasePvalueBY = cell_spec(round(CasePvalueBY, 3),
                           bold = ifelse(CasePvalueBY <= 0.05, TRUE, FALSE),
                           color = ifelse(is.na(CasePvalueBY) == TRUE, "white", "black"),
                           align = "c")
  ) %>%
  kable("latex", digits = 4 , align='l', 
      row.names = FALSE, col.names = colnames,
      booktabs = TRUE, escape = FALSE,
      caption = "Difference in Biomarkers: Controls vs Cases") %>%
      add_header_above(c("","","","Median [IQR]" = 2, "Model" = 3)) %>%
      kable_styling(latex_options = c("hold_position", "scale_down", "repeat_header"),
                    font_size = 8,
                    position = "left") %>%
      column_spec(1, bold = TRUE) %>%
      column_spec(2, italic = TRUE) %>%
      collapse_rows(columns = 1:2,
                    latex_hline = "custom",
                    custom_latex_hline = 1:2,
                    valign = "middle",
                    row_group_label_position = "stack") %>%
  footnote(alphabet = c("Difference of mean for continuous biomarkers; odds ratio for dichotomous biomarkers.",
                        "Weighted linear regression for continuous biomarkers; weighted logistic regression for dichotomous biomarkers. ",
                        "Benjamini & Yekutieli false discovery rate method (2001), 45 tests."))


# Saving html output to separate file: ignore
# kable(t2, digits = 4 , align='r', 
#       row.names = FALSE, col.names = colnames,
#       caption= "Difference in Biomarkers: Controls vs Cases", format = "html") %>%
#       kable_styling("striped", full_width = FALSE) %>%
#       pack_rows("CVL", 1,26, 
#                 label_row_css = "background-color: #666; color: #fff;") %>%
#       pack_rows("Plasma", 27,45, 
#                 label_row_css = "background-color: #666; color: #fff;") %>%
#       save_kable("test.html")
```


```{r heatmap code, include=FALSE}
heatmap <- function (a, b, x, y = "", z, c = 'spearman') {
  # a <- data frame ex. df
  # b <- column for groups ex. df$groups
  # x <- group(s) 
  # y <- title
  # z <- columns that are needed e.g. c(1:5)
  # c <- correlation method; by defualt this is spearman
  
  # Example: heatmap(df, df$group, 1, "Group1", c(3:29,32))
  library(corrplot)
  library(Hmisc)
  #Additional functions needed----
  #Set significant values for p-value using stars
  abbreviate_p <- function(value){  # format string more concisely
    lst = c()
    for (item in value) {
      if (is.nan(item) || is.na(item)) { # if item is NaN return empty string
        lst <- c(lst, '')
        next
      }
      item <- round(item, 3) # round to three digits
      if (item <= 0.001) { # set to ***
        item = '***'
      }
      if (item <= 0.05 & item > 0.01) { # set to *
        item = '*'
      }
      if (item <= 0.01 & item > 0.001) { # set to **
        item = '**'
      }
      if (item > 0.05) { # remove values greater than 0.05
        item = ''
      }  
      item <- as.character(item)
      lst <- c(lst, paste(item))
    }
    return(lst)
  }
  
  options(scipen=999) # turns off scientific notation

  # Get upper triangle of the correlation matrix: p-values
  get_upper_p <- function(cormatrixp){
    cormatrixp[lower.tri(cormatrixp)]<- NA
    return(cormatrixp)
  } 
  
  # Get lower triangle of the correlation matrix: p-values
  get_lower_p <- function(cormatrixp){
    cormatrixp[upper.tri(cormatrixp)]<- NA
    return(cormatrixp)
  } 
  
  # Data setup ----
  d <- a[b %in% x,]   
  
  # Ignore columns that aren’t needed  (group data, PTID, etc)
  cormatrix <- rcorr(as.matrix(d[,z]), type = c)
  
  cormatrixr <- cormatrix$r
  cormatrixp <- get_lower_p(cormatrix$P)
  
  # Create Heatmap ----
  corrplot(cormatrixr, 
           method = "color", type="upper", na.label = " ", title = y, tl.col = "black", 
           tl.cex = 0.5, cl.cex = 0.5,
           col=colorRampPalette(c('#003366',"white",'#b20000'))(200), 
           mar=c(0,0,1,0))
  
  # Add p-values to heatmap
  pos <- expand.grid(1:ncol(cormatrixp), ncol(cormatrixp):1)
  text(pos, abbreviate_p(cormatrixp), cex = 0.5, font = 2)
  
  print(paste ("END"))
  
  options(scipen=0) # turn scientific notation back on

}

```

```{r subset and heatmaps, include=FALSE}

# Subsetting data
## CVL
cvl <- c(grep('CVL' , names(ds) , value = TRUE))
cvl <- c("status", cvl)
cvl <- subset(ds, select = cvl)
colnames(cvl) <- sub("CVL", "", colnames(cvl))

## Plasma
plasma <- c(grep('Plasma' , names(ds) , value = TRUE))
plasma <- c("status", plasma)
plasma <- subset(ds, select = plasma)
colnames(plasma) <- sub("Plasma", "", colnames(plasma))

# Construct heatmaps
pdf("heatmaps/casePlasma.pdf")
heatmap(plasma, plasma$status, "Case", "Recent SV Cases", c(2:20))
dev.off()

pdf("heatmaps/caseCVL.pdf")
heatmap(cvl, cvl$status, "Case", "Recent SV Cases", c(2:27))
dev.off()

pdf("heatmaps/ctrlPlasma.pdf")
heatmap(plasma, plasma$status, "Control", "Recent SV Control", c(2:20))
dev.off()

pdf("heatmaps/ctrlCVL.pdf")
heatmap(cvl, cvl$status, "Control", "Recent SV Control", c(2:27))
dev.off()
```

\newpage
# Heatmaps
## Recent SV Case: Plasma
```{r casePlasma, echo=FALSE}
include_graphics("heatmaps/casePlasma.pdf")
```

\newpage
## Recent SV Control: Plasma
```{r ctrlPlasma, echo=FALSE}
include_graphics("heatmaps/ctrlPlasma.pdf")
```

\newpage
## Recent SV Case: CVL
```{r caseCVL, echo=FALSE}
include_graphics("heatmaps/caseCVL.pdf")
```

\newpage
## Recent SV Control: CVL
```{r ctrlCVL, echo=FALSE}
include_graphics("heatmaps/ctrlCVL.pdf")
```

\newpage


# Table two.2
```{r table two.2 setup, include=FALSE}
# Getting biomarker names for table two ----
biomarkers <- c(grep('CVL|Plasma' , names(ds) , value = TRUE))
biomarkers <- biomarkers[!biomarkers %in% c("PlasmaProgesterone",
                                            'PlasmaEstradiol')]

# Setting biomarker order
bioOrder <- c("TNFa", "IL6", "IL1a", "IL1b", "TGFB", "MIP3a", "IL8", "MCP1", "IP10", "SLPI", "Elafin", "HBD2", "TSP1", "VEGF", "PDGF", "FGF", "CathepsinB", "MMP1", "MMP2", "MMP7", "MMP8", "MMP9", "TIMP1", "TIMP2", "CRP", "Protein")

vals <- NULL
for (n in bioOrder){
  b <- print(grep(n, biomarkers, value = TRUE))
  vals <- c(vals, b)
  rm(b)
  rm(n)
}

biomarkers <- vals

# Subset categorical and Continous biomarkers
catBiomarkers <- c("CVLTGFB", "CVLIP10", "CVLPDGF","CVLFGF", "CVLMMP1", "CVLMMP2",  
                   "PlasmaIL1b", "PlasmaIP10")

nonnormBiomarkers <- biomarkers[!biomarkers %in% catBiomarkers]

# Summary statistics, continuous variables ----------
## Function for the median and IQR stats for the table, 
meanSD <- function(x){
          q <- format(round(mean(x), digits = 2), nsmall = 2)
          r <- format(round(sd(x), digits = 2), nsmall = 2)
          s <- paste0(q," [", r, "]")
          return(s) 
        }

## Making the table
conTab <- ds %>%
           select(c(status,nonnormBiomarkers)) %>%
           group_by(status) %>%
           summarise_at(nonnormBiomarkers, funs(meanSD)) %>%
           select(status, sapply(nonnormBiomarkers, contains)) %>%
           t() %>%
           as.data.frame() 

## Clean the table and make a column
colnames(conTab) <- as.character(unlist(conTab[1,]))
conTab <- conTab[-1,]
conTab$Class <- "Continuous: log(pg/mL)"

# Summary statistics, categorical variables ----------
## Function for the number and pct detectable stats for the table, 
nPCT <- function(x){
  n <- length(x)
  dets <- x == "1"
  dets <- sum(dets, na.rm = TRUE)
  pct <- (dets / n) * 100
  pct <- round(pct, digits = 1)
  s <- paste0(dets," [",pct,"]")
  return(s) 
}

## Making the table
catTab <- ds %>%
  select(c(status,catBiomarkers)) %>%
  group_by(status) %>%
  summarise_at(catBiomarkers, funs(nPCT)) %>%
  select(status, sapply(catBiomarkers, contains)) %>%
  t() %>%
  as.data.frame() 


## Clean the table and make a column
colnames(catTab) <- as.character(unlist(catTab[1,]))
catTab <- catTab[-1,]
catTab$Class <- "Categorical: Detectable [%]"

# Merge two tables together together
t2 <- rbind(conTab, catTab)
t2$Biomarker <- rownames(t2)


# Model: Biomarker = Case Status weighted by Propensity score ----
contDF <- lapply(nonnormBiomarkers, function(x) lm(ds[[x]]~ds$case_status, weights = ds$iptw)) %>%
  lapply(., summary) %>%
  sapply(., function(x) x$coefficients[,c(1,4)]) %>%
  as.data.frame(.)

## Labeling and transforming table
rownames(contDF) <- c("Intercept Estimate", "Case Estimate",
                      "Intercept Pvalue", "CasePvalue")
colnames(contDF) <- nonnormBiomarkers
contDF <- contDF[-c(1,3),]
contDF <- as.data.frame(t(contDF))

# Model: Biomarker = Case Status weighted by Propensity score ----
catDF <- lapply(catBiomarkers , 
                function(x) glm(ds[[x]]~ds$case_status, weights = ds$iptw, family = "binomial")) %>%
  lapply(., summary) %>%
  sapply(., function(x) x$coefficients[,c(1,4)]) %>%
  as.data.frame(.)

## Labeling and transforming table
rownames(catDF) <- c("Intercept Estimate", "Case Estimate",
                     "Intercept Pvalue", "CasePvalue")
colnames(catDF) <- catBiomarkers
catDF <- catDF[-c(1,3),]
catDF <- as.data.frame(t(catDF))
catDF$`Case Estimate` <- exp(catDF$`Case Estimate`)

## p-value adjustments
caseDF <- rbind(contDF, catDF)
caseDF$CasePvalueBY <- p.adjust(caseDF$CasePvalue , method = 'BY')
# caseDF$CasePvalueFDR <- p.adjust(caseDF$CasePvalue , method = 'fdr')

# Merging everything together ----
caseDF$Biomarker <- rownames(caseDF)
t2 <- full_join(t2, caseDF)
rownames(t2) <- t2$Biomarker

write.csv(caseDF, "tables/RSVmodels.csv")
rm(caseDF)

### All data is now in table 2
```

```{r Organize table 2.2, include=FALSE}
# Organize table 2 ---
## Set levels for Class column 
t2$Class <- as.factor(t2$Class)
t2$Class <- relevel(t2$Class, unique(grep("Continuous", t2$Class, value = TRUE)))

## Separate Biomarker column into sample type and biomarker
t2 <- separate(t2, Biomarker, c("Type","Biomarker"), sep ="(?<=CVL)|(?<=Plasma)", remove = FALSE)

# Reorder table 2 ---
order <- c("Type","Class","Biomarker", "Control", "Case", "Case Estimate", "CasePvalue", "CasePvalueBY")
t2 <- t2[,order]
t2 <- t2[order(t2$Type, t2$Class),]
t2 <- t2 %>%
      rename(`Control (N=25)` = Control,
              `Case (N=13)` = Case)

## If I need to reorder the biomarkers I'll probably need to do that here

## Rename categorical rows
write.csv(t2, "tables/Table2.csv")
# Get rid of what we don't need
rm( catBiomarkers, nonnormBiomarkers, 
   order)

## Table two - DONE
```



```{r print table two.2, echo=FALSE}
options(knitr.kable.NA = '')
colnames <- c("","","","Control (N=25)", "Case (N=13)", 
              paste("Case Estimate", footnote_marker_alphabet(1)), 
              paste("P-value", footnote_marker_alphabet(2)), 
              paste("Adjusted P-value", footnote_marker_alphabet(3)))

# Table output
t2 %>%
  mutate(
  Type = cell_spec(Type),
  Class = cell_spec(Class),
  #Biomarker = cell_spec(Biomarker, 
                        #align = ifelse(grepl("(%)", 
                                             #Biomarker,ignore.case = TRUE) == TRUE,"r","l")),
  #Control = cell_spec(Control),
  #Case =  cell_spec(Case),
  `Case Estimate` = cell_spec(round(`Case Estimate`,2),
                          color = ifelse(is.na(CasePvalue) == TRUE, "white", "black"),
                          align = "c"
                          ),
  CasePvalue =  cell_spec(round(CasePvalue, 4), 
                          bold = ifelse(CasePvalue <= 0.05, TRUE, FALSE),
                          color = ifelse(is.na(CasePvalue) == TRUE, "white", "black"),
                          align = "c"
                          ),
  CasePvalueBY = cell_spec(round(CasePvalueBY, 3),
                           bold = ifelse(CasePvalueBY <= 0.05, TRUE, FALSE),
                           color = ifelse(is.na(CasePvalueBY) == TRUE, "white", "black"),
                           align = "c")
  ) %>%
  kable("latex", digits = 4 , align='l', 
      row.names = FALSE, col.names = colnames,
      booktabs = TRUE, escape = FALSE,
      caption = "Difference in Biomarkers: Controls vs Cases") %>%
      add_header_above(c("","","","Mean [SD]" = 2, "Model" = 3)) %>%
      kable_styling(latex_options = c("hold_position", "scale_down", "repeat_header"),
                    font_size = 8,
                    position = "left") %>%
      column_spec(1, bold = TRUE) %>%
      column_spec(2, italic = TRUE) %>%
      collapse_rows(columns = 1:2,
                    latex_hline = "custom",
                    custom_latex_hline = 1:2,
                    valign = "middle",
                    row_group_label_position = "stack") %>%
  footnote(alphabet = c("Difference of mean for continuous biomarkers; odds ratio for dichotomous biomarkers.",
                        "Weighted linear regression for continuous biomarkers; weighted logistic regression for dichotomous biomarkers. ",
                        "Benjamini & Yekutieli false discovery rate method (2001), 45 tests."))


# Saving html output to separate file: ignore
# kable(t2, digits = 4 , align='r', 
#       row.names = FALSE, col.names = colnames,
#       caption= "Difference in Biomarkers: Controls vs Cases", format = "html") %>%
#       kable_styling("striped", full_width = FALSE) %>%
#       pack_rows("CVL", 1,26, 
#                 label_row_css = "background-color: #666; color: #fff;") %>%
#       pack_rows("Plasma", 27,45, 
#                 label_row_css = "background-color: #666; color: #fff;") %>%
#       save_kable("test.html")
```
