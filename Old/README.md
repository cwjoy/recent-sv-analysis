# README #


### What is this repository for? ###

* This is the repository for the Ghosh Recent SV data analysis project.

* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

 Order of R files (and what they do):
 
1. Import Master Database: Takes the Excel file �Recent SV Master� and converts many of the demographic and clinical variables to factors, and saves output as an RDS

2. Subset Premenopausal first visits: takes the RDS created in Import Master Database and removes post-menopausal women and all visits that are not marked 1.  Saves output as RDS

3. Data Transformations: Takes the previous RDS and log transforms laboratory variables.  Drops variables that we decided have too many undetectables.  Transforms variables with >35% undetectables to detectable/nondetectable.  Changes nondetectables in other variables to � lowest detected value.  Saves output as RDS.

4. Analysis: main analysis file. Imports RDS created by Data Transformations.

Supplemental R files: 
Heatmapfunctions: imports the functions we use to create the triangular heatmaps (the third function diffxyHeatmapstaar doesn�t work, don�t use it)
Heatmap prep: creates numeric datasets from the analysis datafile and saves them as RDS files.  Imported later on in the code.

In order to knit the Analysis document you will need to install Mitktex (available here: https://miktex.org/) and Pandoc (available here: https://pandoc.org/). Make sure to choose "Install packages on the fly" (not "Ask Me").

The name of the PDF document will be the name of the R file it is knit from.

Datasets, both the original excel file and any created RDS files live in the data folder.Other folders contain files and graphs from before I learned how to use markdown and knitr to create a single PDF file.  They, and other files in the main folder can be perused at will.


### Who do I talk to? ###

* Christopher Joy or Annette Aldous