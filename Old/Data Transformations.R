#' Data import and transformations
#' 
#' Importing the dataset from the RDS file created by Read Data script
library(MatchIt)
library(tidyverse)
ds=readRDS('../SV HIV/data/Premenopausal First Visit.rds')
ds=as.data.frame(ds)

#' We are not including IL-1a or IL6 in the analysis.  Values will be changed to NA after log transformed
#' All plasma and CVL biomarker values will be log transformed
#' 
#' first we drop variables that are completely or mostly undetectable
drop = c(grep('PlasmaIL6|PlasmaIL1a|PlasmaTNFa' , names(ds) , value = TRUE))
ds = ds[,!(names(ds) %in% drop)]

#' This section applies several data transformations.  
#' The first for loop looks for variables that have more than 35% undetectables and transforms them to factors
biomarkers = grep("Plasma|CVL" , names(ds) , value=TRUE)
for (name in biomarkers) {
  if (class(ds[,name])=="numeric"){
    pctNa = 100*mean(is.na(ds[[name]]))
    if (pctNa>35) ds[[name]] = as.factor(ifelse(is.na(ds[[name]]),'Nondetectable' , 'Detectable'))
  }
}

#' This next loop takes the lowest detected value, divides it by 2 and inserts that into the undetectable fields
for (n in biomarkers) {
  if (class(ds[,n]) == "numeric") {
    x <- min(ds[n], na.rm = TRUE) 
    y <- x/2
    levels(ds[n]) <- c(ds[n], y)
    ds[n][is.na(ds[n])] <- y   
  }
}

#' These steps take the numeric variables in the biomarker data and log-transforms them
oNames = c(grep('Plasma' , names(ds) , value=TRUE))
oNames = oNames[sapply(ds[,oNames],is.numeric)]
ocNames = c(grep('CVL' , names(ds) , value=TRUE))
ocNames = ocNames[sapply(ds[,ocNames],is.numeric)]
ds[,oNames] = log10(ds[,oNames])
ds[,ocNames] = log10(ds[,ocNames])

#' Finally we transform the factor biomarkers into dummy variables.
for (name in biomarkers) {
  if (class(ds[,name])=="factor"){
    ds[,name]=ifelse(ds[,name] == "Detectable" , 1 , 0)
  }
  
}
ds$case_status = plyr::revalue(ds$case_status , c("No"="Control" , "Yes" = "Case"))

#' Several vairables are being transformed into Detectable/Non-detectable 


#' Experimentation and trial in the pmatch script yielded a propensity score based on Age, race, insurance. SMD = 0.001
#' 
#' First we create a dataset that has the variables we will be matching on, plus some additional variables
df = ds %>%
  dplyr::select(Age, thirty_days_partners, abuse_ever_phys, 
         abuse_month_phys, rti_any, VisitNumber, DepressionIndex , case_status)
#' After creation we then create dummy variables for the propensity score matching.  
df$treat = ds$status == "Case"
df$black = ds$Race == "Black"
df$priv_ins = ds$Insurance == "Private"
df$vaginal_condom = ds$vaginal_condom != "No"
df$rti = ds$rti_any == "Positive"

#' The next two procedures calculate the propensity scores and attach them as weights to the data fram containing the dummy variables.  
m.data = matchit(treat ~ Age + black + priv_ins, data=df, method = "full") %>%
  match.data()


#' Add m.data weights to main dataset
ds$weights = m.data$weights
ds$pscore = m.data$distance
ds$iptw = ifelse(m.data$treat==1, 1/ds$pscore,1/(1-ds$pscore))


saveRDS(ds,file='../SV HIV/data/Analysis dataset.rds')